package com.mqtt.mqttapp;

import android.content.Context;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

/**
 * Created by a10441 on 10/7/16.
 */
public class ActionListener implements IMqttActionListener {


    private static final String TAG = "ActionListener";

    /**
     * Actions that can be performed Asynchronously <strong>and</strong> associated with a
     * {@link ActionListener} object
     */
    enum Action {
        /**
         * Connect Action
         **/
        CONNECT,
        /**
         * Disconnect Action
         **/
        DISCONNECT,
        /**
         * Subscribe Action
         **/
        SUBSCRIBE,
        /**
         * Publish Action
         **/
        PUBLISH
    }

    /**
     * The {@link Action} that is associated with this instance of
     * <code>ActionListener</code>
     **/
    private Action action;
    /**
     * The arguments passed to be used for formatting strings
     **/
    private String[] additionalArgs;

//    private Connection connection;
    /**
     * Handle of the {@link Connection} this action was being executed on
     **/
    private String clientHandle;
    /**
     * {@link Context} for performing various operations
     **/
    private Context context;

    /**
     * Creates a generic action listener for actions performed form any activity
     *
     * @param context        The application context
     * @param action         The action that is being performed
     * @param connection     The connection
     * @param additionalArgs Used for as arguments for string formating
     */
    public ActionListener(Context context, Action action,
                           String... additionalArgs) {
        this.context = context;
        this.action = action;
//        this.connection = connection;
//        this.clientHandle = connection.handle();
        this.additionalArgs = additionalArgs;
    }



    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        switch (action) {
            case CONNECT:
                System.out.println("Connected");
//                connect();
                break;
            case DISCONNECT:
                System.out.println("Disconnected");
//                disconnect();
                break;
            case SUBSCRIBE:
                System.out.println("Sub");

//                subscribe();
                break;
            case PUBLISH:
                System.out.println("Published");
//                publish();
                break;
        }
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        switch (action) {
            case CONNECT:
                System.out.println("Error in connecting");
                break;
            case DISCONNECT:
                System.out.println("Disconnected");
//                disconnect(exception);
                break;
            case SUBSCRIBE:
                System.out.println("Subscribed");

//                subscribe(exception);
                break;
            case PUBLISH:
                System.out.println("Publishded err");

//                publish(exception);
                break;
        }
    }
}
