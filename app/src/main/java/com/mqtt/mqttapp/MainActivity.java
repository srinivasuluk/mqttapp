package com.mqtt.mqttapp;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.android.service.MqttTraceHandler;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

//        String clientId = "MQTT_CLIENT_SERVER";
        String clientId = MqttClient.generateClientId();
        final MqttAndroidClient client =
                new MqttAndroidClient(this.getApplicationContext(), "tcp://192.168.0.7:1883",
                        clientId);

        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setUserName("srinivas");
        connOpts.setPassword("srinivas".toCharArray());
//        connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);




//        =====================================

//        mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), serverUri, clientId);
//        client.setCallback(new MqttCallback() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//
//                if (reconnect) {
//                    addToHistory("Reconnected to : " + serverURI);
//                    // Because Clean Session is true, we need to re-subscribe
//                    subscribeToTopic();
//                } else {
//                    addToHistory("Connected to: " + serverURI);
//                }
//            }
//
//            @Override
//            public void connectionLost(Throwable cause) {
//                addToHistory("The Connection was lost.");
//            }
//
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                addToHistory("Incoming message: " + new String(message.getPayload()));
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token) {
//
//            }
//        });

//        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setAutomaticReconnect(true);
//        mqttConnectOptions.setCleanSession(false);







        try {
            //addToHistory("Connecting to " + serverUri);
            client.connect(connOpts, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        client.subscribe("srinivas_test", 0, null, new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                System.out.println("Subscribed!");
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                System.out.println("Failed to subscribe");
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("======================");
//                    addToHistory("Failed to connect to: " + serverUri);
                }
            });



        } catch (MqttException ex){
            ex.printStackTrace();
        }



//        public void subscribeToTopic(){
//            try {
//                clinet.subscribe("srinivas_test", 0, null, new IMqttActionListener() {
//                    @Override
//                    public void onSuccess(IMqttToken asyncActionToken) {
//                        addToHistory("Subscribed!");
//                    }
//
//                    @Override
//                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                        addToHistory("Failed to subscribe");
//                    }
//                });
//
//                // THIS DOES NOT WORK!
//                mqttAndroidClient.subscribe(subscriptionTopic, 0, new IMqttMessageListener() {
//                    @Override
//                    public void messageArrived(String topic, MqttMessage message) throws Exception {
//                        // message Arrived!
//                        System.out.println("Message: " + topic + " : " + new String(message.getPayload()));
//                    }
//                });
//
//            } catch (MqttException ex){
//                System.err.println("Exception whilst subscribing");
//                ex.printStackTrace();
//            }
//        }


//        ============================================





//        connOpts.setCleanSession(true);
        client.setCallback(new MqttCallback() {

            @Override
            public void connectionLost(Throwable cause) { //Called when the client lost the connection to the broker
                System.out.println("Connection Lost");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println(topic + ": " + message);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {//Called when a outgoing publish is complete
                System.out.println("in delivery complete");
            }
        });

//        try {
//
////            IMqttToken token = client.connect(connOpts);
//            client.connect(connOpts, this.getApplicationContext(), new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    System.out.println(asyncActionToken);
//                    System.out.println("Success");
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    System.out.println("Failure: " + exception.getMessage() );
//                    exception.printStackTrace();
//                }
//            });
//            System.out.println(client.isConnected());
////            client.setTraceEnabled(false);
////            client.setTraceCallback(new MqttTraceHandler() {
////                @Override
////                public void traceDebug(String source, String message) {
////                    System.out.println("000000000000000000");
////                }
////
////                @Override
////                public void traceError(String source, String message) {
////                    System.out.println("------------------");
////
////                }
////
////                @Override
////                public void traceException(String source, String message, Exception e) {
////                    System.out.println("=====================");
////
////                }
////            });
//            if(client != null) {
////                client.connect();
////                client.subscribe("srinivas_test", 1);
//                client.subscribe("srinivas_test", 1, this.getApplicationContext(), new IMqttActionListener() {
//                    @Override
//                    public void onSuccess(IMqttToken asyncActionToken) {
//                        System.out.println("Success");
//                    }
//
//                    @Override
//                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                        System.out.println("Error");
//                    }
//                });
//
//                System.out.println(client.isConnected());
//            }
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sencondActivity(View v) {
        Intent intent = new Intent(MainActivity.this,SecnodjeActivity.class);
        startActivity(intent);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()
                ) {
            return true;
        }

        return false;
    }
}
